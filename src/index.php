<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application,
	Bitrix\Main\IO,
	Bitrix\Main\FileTable;

global $USER;
if (!$USER->IsAdmin()) {
	die();
}

define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', true);


\Bitrix\Main\UI\Extension::load("ui.alerts");
\Bitrix\Main\UI\Extension::load("ui.forms");
\Bitrix\Main\UI\Extension::load("ui.progressbar");
\Bitrix\Main\UI\Extension::load("ui.buttons");

class DeleteBadFiles {

	static $doc_root;
	static $upload_dir;
	static $arLogFiles;
	static $mode;
	static $step = 100;
	static $index = 0;
	static $isWork = true;
	static $scanDir = 'iblock';
	static $nextPath = '';
	static $badCount = 0;
	static $allCount = 0;

	public static function init() {
		self::$doc_root = Application::getDocumentRoot();
		self::$upload_dir = \Bitrix\Main\Config\Option::get('main', 'upload_dir');
		self::$arLogFiles = [
			'allTablePaths'	=> new IO\File(__DIR__ . '/allTablePaths.csv'),
			'badIndex'		=> new IO\File(__DIR__ . '/badIndex.csv'),
			'badFiles'		=> new IO\File(__DIR__ . '/badFiles.csv'),
		];

		$request = Application::getInstance()->getContext()->getRequest();

		self::$mode = $request->getPost('log') ?: 'log';
		self::$step = $request->getPost('step') ?: self::$step;
		self::$scanDir = $request->getPost('dir') ?: self::$scanDir;

		if($request->getPost('cleanLog') === 'Y')
		{
			foreach(self::$arLogFiles as $file)
			{
				if($file->isExists())
					$file->putContents('');
			}
		}
	}

	public static function collectFileTable($page = 0)
	{
		$result = [];
		$badId = [];
		$allPaths = [];

		if($page == 0) {
			$allFiles = FileTable::getList([
				'select' => ['ID'],
			])->fetchAll();
			$result['TOTAL'] = count($allFiles);
			unset($allFiles);
		}

		$dbFiles = FileTable::getList([
			'select' => [
				'ID',
				'SUBDIR',
				'FILE_NAME',
			],
			'order' => ['ID'],
			'limit' => self::$step,
			'offset' => self::$step * $page
		]);

		while ($arFile = $dbFiles->fetch()) {
			$fullPath = self::$doc_root . '/' . self::$upload_dir . '/' . $arFile['SUBDIR'] . '/' . $arFile['FILE_NAME'];
			if (!IO\File::isFileExists($fullPath)) {
				$badId[] = $arFile['ID'];
				// CFile::Delete на d7 нет, увы (точнее она как бы есть, но она не работает)
				if(self::$mode !== 'log')
					\CFile::Delete($arFile['ID']);
			} else {
				$allPaths[] = $fullPath;
			}
		}

		if(!empty($badId) && self::$mode === 'log')
			self::$arLogFiles['badIndex']->putContents( implode(";\r\n", $badId).";\r\n", IO\File::APPEND);
		if(!empty($allPaths) && self::$mode === 'log')
			self::$arLogFiles['allTablePaths']->putContents( implode(";\r\n", $allPaths).";\r\n", IO\File::APPEND);

		$result['BAD'] = count($badId);
		$result['PATHS'] = count($allPaths);
		$result['PAGE'] = $page;
		$result['STEP'] = self::$step;
		$result['LOG'] = self::$mode;

		return $result;
	}

	public static function fileCheck($path) {
		$result = [];
		$scan_folder = self::$doc_root . "/". self::$upload_dir;
		if(self::$scanDir !== '')
			$scan_folder .= '/'.self::$scanDir;

		if(!$path)
			$path = $scan_folder;

		if($path !== $scan_folder)
			self::$isWork = false;

		$arStartPath = explode('/', $path);
		$deep = count(explode('/', $scan_folder));

		self::recursiveFileCheck($scan_folder, $deep, $arStartPath);

		$result = [
			'PAGE' => self::$nextPath,
			'STEP' => self::$step,
			'LOG' => self::$mode,
			'DIR' => self::$scanDir,
			'BAD' => self::$badCount,
			'PATHS' => self::$allCount,
		];

		if(self::$nextPath === '')
			$result['CLEAN_DIR'] = self::$mode === 'log' ? 'DONE' : 'Y';

		return $result;
	}

	public static function recursiveFileCheck($scan_folder, $deep, $arStartPath) {
		if(self::$index > self::$step)
			return;
	
		$dir = new IO\Directory($scan_folder);

		// Мы не глубже пути с предыдущего шага или в режиме работы
		if($arStartPath[$deep] || self::$isWork)
		{
			foreach($dir->getChildren() as $path)
			{
				if(self::$nextPath !== '')
					break;

				if(self::$index >= self::$step)
				{
					// Записываем необработанный путь, с него начнётся следующий шаг (обработанный мы могли уже удалить)
					self::$nextPath = $path->getPath();
					self::$index = PHP_INT_MAX;
					break;
				}
	
				if($arStartPath[$deep] === $path->getName() || self::$isWork)
				{
					// Если мы на необходимой глубине, значит это наша папка/файл, можно продолжать обрабатывать
					if(count($arStartPath) === count(explode('/', $path->getPath())))
						self::$isWork = true;
	
					if(get_class($path) === 'Bitrix\Main\IO\Directory')
					{
						self::recursiveFileCheck($path->getPath(), $deep+1, $arStartPath);
					}
					else if (get_class($path) === 'Bitrix\Main\IO\File')
					{
						self::$index++;
						self::$allCount++;
						$arFile = FileTable::getList([
							'select' => ['ID'],
							'filter' => [
								'SUBDIR' => self::getSubDir($path),
								'FILE_NAME' => $path->getName()
							],
							'order' => ['ID'],
						])->fetch();

						if(!$arFile)
						{
							self::$badCount++;
							if(self::$mode !== 'log')
							{
								$path->delete();
							}
							else
							{
								self::$arLogFiles['badFiles']->putContents($path->getPath().";\r\n", IO\File::APPEND);
							}
						}
					}
				}
			}
		}
	}

	public static function cleanEmptyDir($page) {
		$result = [];
		$scan_folder = self::$doc_root . "/". self::$upload_dir;
		if(self::$scanDir !== '')
			$scan_folder .= '/'.self::$scanDir;

		\Bitrix\Main\Diag\Debug::dumpToFile($scan_folder, 'scan_folder', 'log.log');
		self::recursiveCleanEmptyDir($scan_folder);

		$result = [
			'PAGE' => self::$nextPath,
			'STEP' => self::$step,
			'LOG' => self::$mode,
			'DIR' => self::$scanDir,
			'BAD' => self::$badCount,
			'PATHS' => self::$allCount,
			'CLEAN_DIR' => 'DONE',
		];

		return $result;
	}

	public static function recursiveCleanEmptyDir($scan_folder) {
		$dir = new IO\Directory($scan_folder);

		foreach($dir->getChildren() as $path)
		{
			if(get_class($path) === 'Bitrix\Main\IO\Directory')
			{
				self::recursiveCleanEmptyDir($path->getPath());
			}
		}
		if(empty($dir->getChildren()))
		{
			$dir->delete();
			self::$badCount++;
		}
	}

	public static function getSubDir($dir)
	{
		$dirName = $dir->getDirectoryName();
		$upload_dir = self::$doc_root . "/". self::$upload_dir . '/';
		return substr($dirName, strlen($upload_dir));
	}
}

$request = Application::getInstance()->getContext()->getRequest();

if($request->getPost('ajax') === 'Y' && check_bitrix_sessid())
{
	$func = $request->getPost('func') ?: 'collectFileTable';
	$page = $request->getPost('page') ?: 0;
	DeleteBadFiles::init();
	$result = DeleteBadFiles::$func($page);
	echo CUtil::PhpToJSObject($result);
	die();
}

$excludeTitle = '<h3>Папка для зачистки</h3>';
$dir = new IO\Directory(Application::getDocumentRoot() . "/". \Bitrix\Main\Config\Option::get('main', 'upload_dir') ."/");
$excludeTitle .= '<div class="explorer">/'. $dir->getName() .'/</div>';
$excludeHTML = '';
foreach($dir->getChildren() as $path)
{
	if(get_class($path) === 'Bitrix\Main\IO\Directory')
	{
		$excludeHTML = '<label><input type="radio" class="dir" name="dir" value="'. $path->getName() .'"/>/'.$path->getName().'/</label>'.$excludeHTML;
		//recursiveFileCheck($path);
	}
	else if (get_class($path) === 'Bitrix\Main\IO\File')
	{
		/* Да ну их, эти файлы здесь */ 
		//$excludeHTML .= '<label><input type="checkbox" class="file" name="'. $path->getName() .'"value="'. $path->getName() .'"/>/'.$path->getName().'</label>';
	}
}
$excludeHTML = $excludeTitle.$excludeHTML;

?><!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<title>Удаление файлов</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
	<?
	$APPLICATION->ShowCSS(true, false);
	$APPLICATION->ShowHeadStrings();
	?>
	<style>
		body {margin: 5rem;}
		body > div {margin-bottom: 1rem;}
		div.task {line-height: 2rem;}
		#exclude {padding: 1rem; border: 1px solid var(--ui-alert-background); overflow: auto; max-height: 20rem;}
		#exclude.bad {border-color: var(--ui-field-color-danger)};
		#exclude h3 {margin-top: 0;}
		.explorer {line-height: 2rem; font-size: 1.1rem; padding: 0rem 1rem; background-color: var(--ui-alert-background);}
		#exclude label {display: block; line-height: 1.8rem;}
		.hide {display: none;}
	</style>
</head>
<body>
	<h1>Удаление файлов</h1>
	<div id="inputs">
		<div class="ui-alert ui-alert-primary">
			<span class="ui-alert-message">Если файлов очень много, лучше использовать пошаговый режим.</span>
		</div>
		<div class="ui-ctl ui-ctl-textbox">
			<div class="ui-ctl-tag ui-ctl-tag-primary">элементов за шаг</div>
			<input type="text" id="step" name="step" class="ui-ctl-element" value="100">
		</div>
	</div>
	<div id="exclude"><?=$excludeHTML?></div>
	<div id="tasks"></div>
	<div id="upload"></div>
	<div id="pbar"></div>

	<div id="controls"></div>
</body>
<script>
(function() {
	var badIds = 0;
	var allPaths = 0;
	var badFiles = 0;
	var allFiles = 0;
	var button = new BX.UI.Button({
		text: "Выполнить",
		dropdown: true,
		menu: {
			items: [
				{
					text: "Собрать файлы из таблицы",
					onclick: function(event, item){
						item.getMenuWindow().close();
						scanTableFiles(false);
					}
				},
				{
					text: "Найти незадействованные файлы",
					onclick: function(event, item){
						item.getMenuWindow().close();
						scanFileDir(false);
					}
				},
				{
					text: "Удалить ошибочные записи из таблицы",
					onclick: function(event, item) {
						item.getMenuWindow().close();
						scanTableFiles(true);
					}
				},
				{
					text: "Удалить незадействованные файлы",
					onclick: function(event, item) {
						item.getMenuWindow().close();
						scanFileDir(true);
					}
				}
			],
			closeByEsc: true,
			offsetTop: 4,
		},
	});
	var container = document.getElementById("controls");
	button.renderTo(container);
	var step = document.getElementById("step");
	var pBar = new BX.UI.ProgressBar({ 
		value: 0,
		statusType: BX.UI.ProgressBar.Status.COUNTER
	});
	var dirsInput = document.getElementsByName('dir');

	function scanTableFiles(bClean) {
		badIds = 0;
		allPaths = 0;
		BX.cleanNode(BX('tasks'));
		button.setDisabled(true);

		var post = {};
		post['step'] = step.value;
		post['page'] = 0;
		post['func'] = 'collectFileTable';
		post['cleanLog'] = 'Y';
		post['log'] = bClean ? 'clean' : 'log';
		post['ajax'] = 'Y';
		post['sessid'] = BX.bitrix_sessid();

		var ajaxUrl = "<?=$request->getRequestedPageDirectory()?>/";

		BX.ajax({
			'url': ajaxUrl,
			'method': 'POST',
			'dataType': 'json',
			'data': post,
			'onsuccess': badFilesHandler,
		});
	}

	function badFilesHandler(result) {
		var obResult = BX.parseJSON(result);

		var page = parseInt(obResult.PAGE, 10);
		var step = parseInt(obResult.STEP, 10);
		var badIdsMess = BX('badId');
		var allPathsMess = BX('allPaths');
		badIds += parseInt(obResult.BAD, 10);
		allPaths += parseInt(obResult.PATHS, 10);

		if(page == 0)
		{
			badIdsMess = BX.create({
				tag: 'div',
				props: {id: 'badId', className: 'task'},
				text: 'Ошибок в таблице: ' + badIds,
			});
			allPathsMess = BX.create({
				tag: 'div',
				props: {id: 'allPaths', className: 'task'},
				text: 'Существующих файлов: ' + allPaths,
			});
			BX.append(badIdsMess, BX('tasks'));
			BX.append(allPathsMess, BX('tasks'));
			pBar.maxValue = parseInt(obResult.TOTAL, 10);
			pBar.update(step);
			BX.append(pBar.getContainer(), BX('pbar'));
		} else {
			pBar.update(step * (page + 1));
			badIdsMess.innerText = 'Ошибок в таблице: ' + badIds;
			allPathsMess.innerText = 'Существующих файлов: ' + allPaths;
		}

		if(step * (page + 1) >= pBar.maxValue)
		{
			button.setDisabled(false);
		} else {
			var post = {};
			post['step'] = step;
			post['page'] = page + 1;
			post['func'] = 'collectFileTable';
			post['log'] = obResult.LOG ? obResult.LOG : 'log';
			post['cleanLog'] = 'N';
			post['ajax'] = 'Y';
			post['sessid'] = BX.bitrix_sessid();

			var ajaxUrl = "<?=$request->getRequestedPageDirectory()?>/";
	
			BX.ajax({
				'url': ajaxUrl,
				'method': 'POST',
				'dataType': 'json',
				'data': post,
				'onsuccess': badFilesHandler,
			});
		}
	}

	function scanFileDir(bClean) {
		var dir = '';
		badFiles = allFiles = 0;
		for(var d in dirsInput)
		{
			if(dirsInput.hasOwnProperty(d))
			{
				if(dirsInput[d].checked)
					dir = dirsInput[d].value;
			}
		}
		if(dir === '')
		{
			document.getElementById("exclude").classList.add('bad');
			return;
		} else {
			document.getElementById("exclude").classList.remove('bad');
		}

		BX.cleanNode(BX('tasks'));
		button.setDisabled(true);

		var post = {};

		post['step'] = step.value;
		post['dir'] = dir;
		post['func'] = 'fileCheck';
		post['cleanLog'] = 'Y';
		post['log'] = bClean ? 'clean' : 'log';
		post['ajax'] = 'Y';
		post['sessid'] = BX.bitrix_sessid();

		var ajaxUrl = "<?=$request->getRequestedPageDirectory()?>/";

		BX.ajax({
			'url': ajaxUrl,
			'method': 'POST',
			'dataType': 'json',
			'data': post,
			'onsuccess': scanDirHandler,
		});
	}

	function scanDirHandler(result) {
		var obResult = BX.parseJSON(result);

		var page = obResult.PAGE;
		var dir = obResult.DIR;
		var cleanDir = obResult.CLEAN_DIR;
		var step = parseInt(obResult.STEP, 10);
		var badFilesMess = BX('badFiles');
		var allFilesMess = BX('allFiles');
		var deletedDirs;
		if(cleanDir !== 'DONE')
			badFiles += parseInt(obResult.BAD, 10);
		allFiles += parseInt(obResult.PATHS, 10);

		if(badFilesMess === null)
		{
			badFilesMess = BX.create({
				tag: 'div',
				props: {id: 'badFiles', className: 'task'},
				text: 'Несвязанных файлов обработано: ' + badFiles,
			});
			allFilesMess = BX.create({
				tag: 'div',
				props: {id: 'allFiles', className: 'task'},
				text: 'Всего файлов обработано: ' + allFiles,
			});
			BX.append(badFilesMess, BX('tasks'));
			BX.append(allFilesMess, BX('tasks'));
			// Ну их, эти бары, файлы ещё все считать заранее
			//pBar.maxValue = parseInt(obResult.TOTAL, 10);
			//pBar.update(step);
			//BX.append(pBar.getContainer(), BX('pbar'));
		} else {
			//pBar.update(step * (page + 1));
			badFilesMess.innerText = 'Несвязанных файлов обработано: ' + badFiles;
			allFilesMess.innerText = 'Всего файлов обработано: ' + allFiles;
		}

		if(page === '' && cleanDir == 'DONE')
		{
			deletedDirs = BX.create({
				tag: 'div',
				props: {id: 'deletedDirs', className: 'task'},
				text: 'Удалено пустых директорий: ' + badFiles,
			});
			BX.append(deletedDirs, BX('tasks'));
			button.setDisabled(false);
		} else if (cleanDir == 'Y') {
			var post = {};
			post['page'] = page;
			post['dir'] = dir;
			post['func'] = 'cleanEmptyDir';
			post['cleanLog'] = 'N';
			post['ajax'] = 'Y';
			post['sessid'] = BX.bitrix_sessid();

			var ajaxUrl = "<?=$request->getRequestedPageDirectory()?>/";
	
			BX.ajax({
				'url': ajaxUrl,
				'method': 'POST',
				'dataType': 'json',
				'data': post,
				'onsuccess': scanDirHandler,
			});
		} else {
			var post = {};
			post['step'] = step;
			post['page'] = page;
			post['dir'] = dir;
			post['func'] = 'fileCheck';
			post['log'] = obResult.LOG ? obResult.LOG : 'log';
			post['cleanLog'] = 'N';
			post['ajax'] = 'Y';
			post['sessid'] = BX.bitrix_sessid();

			var ajaxUrl = "<?=$request->getRequestedPageDirectory()?>/";
	
			BX.ajax({
				'url': ajaxUrl,
				'method': 'POST',
				'dataType': 'json',
				'data': post,
				'onsuccess': scanDirHandler,
			});
		}
	}
})();
</script>